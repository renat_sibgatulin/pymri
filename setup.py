from setuptools import setup

setup(
    name="mriutils",
    version="0.1",
    description="MRI data meet labeled arrays",
    author="Renat Sibgatulin",
    author_email="renat.sibgatulin@gmail.com",
    license="MIT",
    packages=["mriutils"],
    install_requires=[
        "numpy",
        "nibabel",
        "xarray",
        "scipy",
        "scikit-image",
        "scikit-learn",
        "pandas",
        "pydicom",
        "pympm",
    ],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "detect_phase_residues = mriutils.cli.detect_residues:main",
        ],
    },
    zip_safe=False,
)
