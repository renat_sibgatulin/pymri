import pytest
from pymri.io import _choose_dims


@pytest.mark.parametrize(
    "dims, preference, expected",
    [
        ({"descr": ("red", "lie"), "affine": ("R", "A")}, "descr", ("red", "lie")),
        ({"descr": ("red", "lie"), "affine": ("R", "A")}, "affine", ("R", "A")),
        ({"descr": None, "affine": ("R", "A")}, "descr", ("R", "A")),
        ({"descr": None, "affine": ("R", "A")}, "affine", ("R", "A")),
        ({"descr": ("red", "lie"), "affine": None}, "descr", ("red", "lie")),
        ({"descr": ("red", "lie"), "affine": None}, "affine", ("red", "lie")),
    ],
)
def test_choose_dims(dims, preference, expected):
    actual = _choose_dims(dims, preference)
    assert actual == expected


@pytest.mark.parametrize(
    "dims, preference, expected",
    [
        ({"descr": None, "affine": None}, "descr", ("red", "lie")),
        ({"descr": None, "affine": None}, "affine", ("red", "lie")),
    ],
)
def test_choose_dims_fails(dims, preference, expected):
    with pytest.raises(ValueError):
        _choose_dims(dims, preference)
