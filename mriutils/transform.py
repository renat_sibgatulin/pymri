import itertools
import numpy as np
import xarray as xr
import numpy.linalg as npl
from nibabel.affines import apply_affine

def interpolate_to_volume(source, target, ijk_source_dims, ijk_target_dims, **kwargs):
    """ Interpolate the source array to the space of the target array 
        Parameters
        ----------
            source: xarray.DataArray.
                Represents the data to be interpolated.
                Must contain affine transform from the ijk-voxel space to RAS+ space in attrs
            target: xarray.DataArray.
                Represents the space to which source is to be interpolated
                Must contain affine transform from the ijk-voxel space to RAS+ space in attrs
            ijk_*_dims: array-like.
                Represents the dims of the corresponding array as they label [i,j,k] in voxel space
                Worning: must not be named ['i','j','k'], otherwise wil corresponding coordinates will be droped
            method: string, optional. 
                {‘linear’, ‘nearest’}, passed to xr.Dataset.interp. Default: 'linear'
            fill_value: number, optional.
                If provided, the value to use for points outside of the interpolation domain. 
                If None, values outside the domain are extrapolated. Extrapolation is not supported by method “splinef2d”.
                Passed to sp.interpolate.interpn. Default: nan
        Returns
        -------
            source_interpolated: xarray.DataArray
            trg2src_vox: numpy.array, (4,4).
                Represents transform from the ijk-voxel space of the target image to the ijk-voxel space of the source
    """
    assert len(set(ijk_source_dims)) == 3, 'Source dims must include 3 distinct dim names'
    assert len(set(ijk_target_dims)) == 3, 'Target dims must include 3 distinct dim names'

    # TO DO: look in this one. Might be bullshit to be eliminated. Certainly does not work
    # if not ijk_source_dims == ijk_target_dims:
    #     source = source.transpose(*ijk_target_dims)
    #     source.attrs['affine'] = permute_affine(source.attrs['affine'], ijk_source_dims, ijk_target_dims)

    rename_dict = {src_dim: ijk_dim for src_dim, ijk_dim in zip(ijk_source_dims, ['i','j','k'])}
    ds = source.rename(rename_dict).to_dataset(name='src')
    trg2src_vox = npl.inv(source.attrs['affine']).dot(target.attrs['affine'])

    ijk_target = tuple(target.coords[d].data for d in ijk_target_dims) # tuple of 0D arrays
    pts_ijk_target = np.asarray(list(itertools.product(*ijk_target))) # (n,3) array with all points
    pts_ijk_src_resampled = apply_affine(trg2src_vox, pts_ijk_target)

    i_src_resampled = xr.DataArray(pts_ijk_src_resampled[:,0].reshape([target.sizes[d] for d in ijk_target_dims]),
                                   dims=ijk_target_dims, 
                                   coords={d:target.coords[d] for d in ijk_target_dims})
    j_src_resampled = xr.DataArray(pts_ijk_src_resampled[:,1].reshape([target.sizes[d] for d in ijk_target_dims]),
                                   dims=ijk_target_dims, 
                                   coords={d:target.coords[d] for d in ijk_target_dims})
    k_src_resampled = xr.DataArray(pts_ijk_src_resampled[:,2].reshape([target.sizes[d] for d in ijk_target_dims]),
                                   dims=ijk_target_dims, 
                                   coords={d:target.coords[d] for d in ijk_target_dims})
    dsi = ds.interp(i=i_src_resampled, j=j_src_resampled, k=k_src_resampled, **kwargs)
    dsi.src.attrs['affine'] = target.attrs['affine']

    return dsi.src.drop(['i','j','k']), trg2src_vox