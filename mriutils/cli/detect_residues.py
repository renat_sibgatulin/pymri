import itertools
from functools import partial

import click
import jax
import jax.numpy as np
import nibabel as nib


def wrap(array):
    return np.mod(array + np.pi, 2 * np.pi) - np.pi


@partial(jax.jit, static_argnums=1)
def circular_difference(array, axis):
    return array - np.roll(array, 1, axis=axis)


@jax.jit
@partial(np.vectorize, signature="(n,m,k)->(n,m,k)")
def detect_residues(array):
    grad = wrap(np.stack([circular_difference(array, ax) for ax in range(array.ndim)]))
    axes_paris = itertools.combinations(range(array.ndim), 2)
    curl = np.stack(
        [
            circular_difference(grad[i], j) - circular_difference(grad[j], i)
            for i, j in axes_paris
        ],
    )
    return (curl / (2 * np.pi)).astype(int).any(axis=0)


def apply_to_trailing_dims(func, array):
    ndim = array.ndim
    if ndim == 3:
        return func(array)
    grid_shape = array.shape[:3]
    trail_axes = tuple(range(3, ndim))
    n_trailing_dim = ndim - 3
    click.echo(
        f"Input {array.shape=}, assuming {grid_shape=} and "
        f"{n_trailing_dim} trailing dimension(s) to loop over"
    )

    result_transposed = func(array.transpose(trail_axes + (0, 1, 2)))

    return result_transposed.transpose((-3, -2, -1) + tuple(range(n_trailing_dim)))


@click.command
@click.argument("input_phase")
@click.argument("output")
def main(input_phase, output):
    img = nib.nifti1.load(input_phase)
    phase = img.get_fdata()
    result = apply_to_trailing_dims(detect_residues, phase)
    nib.nifti1.Nifti1Image(result, img.affine, img.header).to_filename(output)


if __name__ == "__main__":
    main()
