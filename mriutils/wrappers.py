import numpy as np
import xarray as xr
import pandas as pd
from scipy.optimize import curve_fit
from skimage.restoration import unwrap_phase


def lin_fit(da, dim_fit, return_std=False):
    dim_intact = [d for d in da.dims if d != dim_fit]
    try:
        n_points = da.sizes[dim_fit]
    except KeyError:
        raise ValueError(f"DataArray has to have a dimension {dim_fit}")
    if return_std:
        fit, cov = np.polyfit(
            da.coords[dim_fit].data,
            da.transpose(dim_fit, *dim_intact).data.reshape((n_points, -1)),
            1,
            cov=True,
        )
    else:
        fit = np.polyfit(
            da.coords[dim_fit].data,
            da.transpose(dim_fit, *dim_intact).data.reshape((n_points, -1)),
            1,
            cov=False,
        )
    da_fit = xr.DataArray(
        fit.reshape([2] + [da.sizes[d] for d in dim_intact]),
        dims=["fit"] + dim_intact,
        coords={d: da.coords[d] for d in dim_intact},
        attrs=da.attrs,
    )
    da_fit.coords["fit"] = ["slope", "offs"]
    if return_std:
        da_std = xr.DataArray(
            np.sqrt(cov[np.eye(2, dtype=bool), ...]).reshape(
                [2] + [da.sizes[d] for d in dim_intact]
            ),
            dims=["fit"] + dim_intact,
            coords={d: da.coords[d] for d in dim_intact},
            attrs=da.attrs,
        )
        da_std.coords["fit"] = ["slope", "offs"]
        output = xr.concat([da_fit, da_std], dim="fit_results")
        output.coords["fit_results"] = ["coeff", "std"]
        return output
    else:
        return da_fit


def unwrap_phase_xr(phase, broadcast_dim):
    assert broadcast_dim in phase.dims, "dim not found in input array dimensions"
    dims_init = phase.dims
    dims_targ = (broadcast_dim,) + tuple(d for d in dims_init if d != broadcast_dim)
    phase_unwr = phase.transpose(*dims_targ).copy()
    phase_unwr.data = np.array([unwrap_phase(p) for p in phase_unwr.data])
    return phase_unwr.transpose(*dims_init)


def nonlin_fit(magn, mask, linfit, dim_fit):
    def exp_2param_squared(t, s0, r2_star):
        return s0 ** 2 * np.exp(-2 * r2_star * t)

    def fit_pandas_row(idx, row, te):
        """ Meant to be iterated across multi-index-ed rows
        Parameters
        ----------
            idx:    a tuple corresponding to the intact, linearized dims
            row:    contains one column per echo in te, mask and the init guess for parameters
            te:     vector of echo times, must match names of the columns in row
        """
        try:
            (s0, r2_star), pcov = curve_fit(
                exp_2param_squared,
                te,
                row[te].values.astype(
                    float
                ),  # for some reason subindexing with a list leads to an array of objects
                p0=row[["s0", "r2_star"]].values.astype(float),
            )
            s0_se, r2_star_se = np.sqrt(np.diag(pcov))
        except RuntimeError:
            s0, r2_star, s0_se, r2_star_se = np.nan, np.nan, np.inf, np.inf
        columns = {
            "s0": s0,
            "r2_star": r2_star,
            "s0_se": s0_se,
            "r2_star_se": r2_star_se,
        }
        multi_index = pd.MultiIndex.from_tuples([idx], names=dim_intact)
        return pd.DataFrame(columns, index=multi_index)

    # Prepare the data
    dim_intact = [d for d in magn.dims if d != dim_fit]
    df_tidy = (magn ** 2).stack(lin=dim_intact).to_dataset(dim_fit).to_dataframe()
    df_tidy["mask"] = mask.stack(lin=dim_intact).to_pandas()
    df = pd.concat(
        [df_tidy, linfit.stack(lin=dim_intact).to_pandas().T], axis=1
    )  # don't yet understand why to_pandas flips dimensions

    df_masked = df[df["mask"] == 1]  # that line trims the data to the mask
    df_fit = pd.concat(
        [
            fit_pandas_row(idx, row, magn.coords[dim_fit].data)
            for idx, row in df_masked.iterrows()
        ],
        axis=0,
    )
    ds = xr.Dataset.from_dataframe(
        df_fit
    )  # the resulting DataArrays have different (smaller) shapes
    # ds.attrs = magn.attrs
    return ds
