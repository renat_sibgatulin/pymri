import numpy as np
import nibabel as nib
from mriutils.io import load_nii
from mriutils.transform import interpolate_to_volume


class NiiResampler:
    def __init__(self, target_name, spatial_dims=None):
        print(f"loading {target_name}")
        self.target = load_nii(target_name)
        if spatial_dims is None:
            if self.target.ndim == 3:
                self.spatial_dims = self.target.dims
            else:
                ValueError(
                    f"target.ndim == {self.target.ndim}, spatial_dims must be provided"
                )

    def __call__(
        self,
        source_inp_name,
        source_out_name,
        affine_out_name=None,
        source_spatial_dims=None,
    ):
        print(f"loading {source_inp_name}")
        source_img = nib.load(source_inp_name)
        source = load_nii(source_inp_name)
        if source_spatial_dims is None and source_img.ndim == 3:
            source_spatial_dims = source.dims
        assert (
            source_img.ndim == 3 or len(source_spatial_dims) == 3
        ), "Only 3D images are supported so far. Go ahead and add the support"

        print(f"resampling {source_inp_name}")
        res, trg2src_affine = interpolate_to_volume(
            source,
            self.target,
            ijk_source_dims=source_spatial_dims,
            ijk_target_dims=self.spatial_dims,
            method="nearest",
        )

        if not affine_out_name is None:
            print(f"writing to {affine_out_name}")
            np.savetxt(affine_out_name, trg2src_affine)

        print(f"writing to {source_out_name}")
        if isinstance(source_img.header, nib.nifti1.Nifti1Header):
            source_header = self.prepare_output_nifti_header(source_img.header)
        else:
            source_header = None
        nii = nib.Nifti1Image(res.data, self.target.attrs["affine"], source_header)
        nii.to_filename(source_out_name)

    def prepare_output_nifti_header(self, source_header):
        source_header.set_sform(self.target.attrs["affine"], code="aligned")
        ndims = len(source_header.get_data_shape())
        zooms = np.ones((ndims,))
        zooms[:3] = nib.affines.voxel_sizes(self.target.attrs["affine"])
        source_header.set_zooms(zooms)
        source_header.set_data_dtype("float64")
        return source_header
