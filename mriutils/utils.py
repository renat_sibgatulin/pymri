import pandas as pd
import xarray as xr
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from itertools import combinations_with_replacement


def polyfit(array, degree, dims=None, verbose=False):
    """
    Parameters
    ----------
    array : xr.DataArray
        Data to be fitted. Irrelevant values should be set to NaN,
    dims : list, optional
        The dims to fit over. If not given, all dims are used

    Returns
    -------
    fitted : xr.DataArray
        the result of the fit
    df_coef : pd.DataFrame
        coefficients of the fit with labels
    """
    if dims is None:
        dims = list(array.dims)
    df = array.to_dataframe(name="data").dropna().reset_index()

    # xr.DataArray.to_dataframe seems to ignore .coords.dtype.
    # Correspondingly, X.dtype needs to be set explicitly
    y = df.loc[:, "data"].values
    X = df.loc[:, dims].values.astype(y.dtype)  # workaround

    poly = PolynomialFeatures(degree=degree)
    if verbose:
        print("Transforming predictors")
    X_t = poly.fit_transform(X)

    regressor = LinearRegression()
    if verbose:
        print("Fitting the model")
    regressor.fit(X_t, y)
    if verbose:
        print("Predicting fitted offset")
    y_predict = regressor.predict(X_t)
    if verbose:
        print("Reshaping the fitted offset")
    df = df.assign(fit=y_predict)
    ds = df.set_index(dims).to_xarray()  # what happens to other dims?
    fitted = xr.align(ds.fit, array, join="outer")[0]

    if verbose:
        print("Building DataFrame with the coefficients")
    if degree > 1:
        coef_labels = (
            [(0, "bias")]
            + [(1, d) for d in dims]
            + [
                (n, "".join(pair))
                for n in range(2, degree + 1)
                for pair in combinations_with_replacement(dims, n)
            ]
        )
    elif degree == 1:
        coef_labels = [(0, "bias")] + [(1, d) for d in dims]
    else:
        ValueError(f"degree must be >=1, got {degree}")

    idx = pd.MultiIndex.from_tuples(coef_labels, names=("order", "coef"))

    df_coefs = pd.DataFrame(
        regressor.coef_, index=idx, columns=["coef_value"]
    )
    df_coefs.loc[(0, "bias"), "coef_value"] = regressor.intercept_

    return fitted, df_coefs
