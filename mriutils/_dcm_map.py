from collections import OrderedDict
import numpy as np

""" Defines a mapping from a DICOM field to an xarray dimension """

def default_sorting_func(field_values):
    """ Implements the simplest mapping of the field values into a segment [0..N_uniqe_values] 
    Parameters
    ----------
    field_values : a list of values obtained via fetch_func from every DICOM in the dicom.dataset.FileDataset list

    Returns
    -------
    mapping : a dict mapping unique_scalar_value: unique_index
    """
    values = sorted(set(field_values))
    indices = range(len(values))
    return OrderedDict(zip(values, indices))

def fetch_func_factory(field_name):
    """ Produces a function (closure) simply returning the value of field_name DICOM tag """
    def default_fetch_func(dcm):
        """ Implements the simplest case of value fetching
            simply accesses predefined DICOM tag (field_name)
        Parameters
        ----------
        dcm : a dicom.dataset.FileDataset object
        """
        return dcm.data_element(field_name).value
    return default_fetch_func

def fetch_slice_location(dcm):
    image_position_patient = np.array(dcm.data_element('ImagePositionPatient').value)
    image_orientation_patient = np.array(dcm.data_element('ImageOrientationPatient').value)
    slice_location = np.dot(image_position_patient,np.cross(image_orientation_patient[:3],image_orientation_patient[3:]))
    return slice_location