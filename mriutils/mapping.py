# import pympm
import numpy as np
import xarray as xr
from mriutils import wrappers
from scipy.optimize import least_squares

# def compute_t1(pdw, t1w, mask=None, relative_fa=None, method='rational_2nd_order'):
#     """ Compute T1 map from two images acquired with different flip angle using a certain approximation of Ernst equation
#     Parameters
#     ----------
#         pdw,t1w:        xr.DataArray
#                         The magnitude of the first/second image. The `attrs` dict has to have 'FA' and 'TR'
#                         values set to the nominal flip angle and the repetition time correspondingly (both scalar)
#         mask:           xr.DataArray, optional
#                         Brain mask. T1 values outside of the mask are set to NaN
#         relative_fa:    xr.DataArray, optional
#                         Fraction of the nominal flip angle mapped with a separate sequence.
#                         If present, used to compute the flip angle used in further computations
#         method:         {'rational', 'rational_2nd_order', 'exp_expanded', 'same_tr'}, optional
#                         Specifies the approximaiton of Ernst equation used

#     Returns
#     -------
#         t1:             xr.DataArray,
#                         the T1 relaxation time map in the same units as the repetition time

#     Reference: Helms G, Dathe H, Dechent P. Quantitative FLASH MRI at 3T using a rational
#         approximation of the Ernst equation. Magnetic Resonance in Medicine: An Official
#         Journal of the International Society for Magnetic Resonance in Medicine.
#         2008 Mar;59(3):667-72.
#     """
#     compute_t1_methods = {
#         'rational': pympm.t1.compute_t1_rational_approximation,
#         'rational_2nd_order': pympm.t1.compute_t1_rational_2nd_order_approximation,
#         'same_tr': pympm.t1.compute_t1_same_tr,
#         'exp_expanded': pympm.t1.compute_t1_exp_expanded
#     }

#     fa_pdw, fa_t1w = (np.deg2rad(s.attrs['FA']) for s in (pdw, t1w))
#     if relative_fa is not None:
#         fa_pdw *= relative_fa
#         fa_t1w *= relative_fa

#     t1 = compute_t1_methods[method](pdw, fa_pdw, pdw.attrs['TR'],
#                                     t1w, fa_t1w, t1w.attrs['TR'])
#     if mask is not None:
#         # xr.where instead of direct indexing with numpy arrays for the sake of xr broadcasting
#         t1 = xr.where(mask, t1, np.nan)

#    return t1

# def compute_mt_saturation(mtw, pd_star, r1_rec_s, relative_fa=None, method='rational_2nd_order'):
#     """ Compute MT saturation as described in [g._erratum_2010]
#     Parameters
#     ----------
#         mtw: 3D ndarray of floats
#             Represents the data acquired with MT weighting
#         tr_mtw_s: float,
#             Repetition time of the MT weighting acquisition (in seconds)
#         pd_star: 3D ndarray of floats
#             Whatever it is
#         r1_rec_s: 3D ndarray of floats
#             T1 relaxation rate (in inverse seconds)
#         relative_fa: 3D ndarray of floats
#             Actual flip angle of the MT weighting acquisition estimated as a fraction of the nominal FA
#         method: str,
#             Either 'rational', or 'rational_2nd_order'
#     Returns
#     -------
#         mts: 3D ndarray of floats
#             MT saturation in percents
#     """
#     fa_mtw_rad = np.deg2rad(mtw.attrs['FA'])
#     # Correct for transmit bias (if the approximation is inhomogeneous)
#     if relative_fa is not None:
#         fa_pdw *= relative_fa
#         fa_t1w *= relative_fa

#     compute_mts_methods = {
#         'rational': pympm.compute_mts_rational_approximation,
#         'rational_2nd_order': pympm.compute_mts_rational_2nd_order_approximation,
#     }

#     mts = 1e2 * compute_mts_methods[method](mtw, fa_mtw_rad, mtw.attrs['TR'], pd_star, r1_rec_s) # units: %

#     if mask is not None:
#         # xr.where instead of direct indexing with numpy arrays for the sake of xr broadcasting
#         mts = xr.where(mask, mts, np.nan)

#     return mts


def r2_star_lin_fit(da):
    da_log = np.log(da)
    da_log = xr.where(np.isinf(da_log), 0, da_log)
    fit = wrappers.lin_fit(da_log, dim_fit="echo")
    s0 = np.exp(fit.sel(fit="offs", drop=True))
    r2_star = np.clip(-fit.sel(fit="slope", drop=True), 0, None)
    return s0, r2_star


def nonlinear_fit(
    da, model, p0, dim_fit="echo", p_bound=([0, 0], [np.inf, np.inf])
):
    def resid(p, x, y):
        return model(p, x) - y

    df = da.to_dataframe().dropna()
    dim_intact = [d for d in da.dims if d != dim_fit]
    df_grouped = df.groupby(dim_intact)

    # Prepare the closure to fit
    x = da.coords[dim_fit].data

    def fun(y):
        return least_squares(
            resid,
            p0,
            loss="soft_l1",
            f_scale=0.1,
            args=(x, y),
            bounds=p_bound,
        )

    # The fit itself
    df_fit = df_grouped[da.name].apply(fun).to_frame()

    return df_fit


def r2_star_nonlinear_nonnegative_fit(da, p0=[500, 50]):
    def model(p, t):
        return p[0] * np.exp(-p[1] * t)

    df_fit = nonlinear_fit(
        da, model, p0, dim_fit="echo", p_bound=([0, 0], [np.inf, np.inf])
    )

    # Prepare the output
    df_fit.index.set_names(level=-1, names="fit", inplace=True)
    df_res = df_fit.xs("x", level=-1)
    df_res.loc[:, "r2_star"] = [x[1] for x in df_res[da.name].values]
    df_res.loc[:, "s0"] = [x[0] for x in df_res[da.name].values]
    df_res.drop(columns=da.name, inplace=True)
    df_res = df_res.assign(success=df_fit.xs("success", level=-1))

    return df_res.to_xarray()
