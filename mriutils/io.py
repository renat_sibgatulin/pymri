import os
import re
import pydicom
import itertools
import numpy as np
import xarray as xr
import nibabel as nib
from typing import Literal, Optional
from collections import OrderedDict
from mriutils import _dcm_map as dm


PREF = Literal["descr", "affine"]


def export(filename, array, affine, header):
    print(f"Saving {filename}")
    img_out = nib.Nifti1Image(array, affine, header)
    img_out.to_filename(filename)


def extract_dicom_field(dicom_folder, field_name):
    """Returns unique values of DICOM field given a folder of DICOM files

    Note: numerical values like TE are stored in DICOM as strings. Here
    type casting is outsorced to numpy and in general is not guaranteed.
    Also mind the units of the returned values
    """
    dcms = [
        pydicom.read_file(f"{dicom_folder}/{file}", force=True)
        for file in os.listdir(dicom_folder)
    ]
    values = np.unique([dcm.data_element(field_name).value for dcm in dcms])
    return values


def _dims_from_affine(affine, ndims: int) -> tuple[str]:
    assert ndims < 5, "cannot infer more than 4 dims from an affine transform"
    dims_default = nib.aff2axcodes(affine) + ("echo",)
    dims = dims_default[:ndims]
    return dims


def _dims_from_descr(description: bytes, ndims: Optional[int] = None) -> tuple[str]:
    pattern = re.compile(rb"^\w{3}(,\w{3})*$")
    assert pattern.match(description), f"{description} doesn't match expected pattern"
    dims = description.decode("utf-8").split(",")
    if ndims is not None:
        assert (
            len(dims) == ndims
        ), f"header's description ({description}) disagrees with {ndims=}"
    return tuple(dims)


def _choose_dims(dims: dict[PREF], preference: PREF) -> tuple[str]:
    assert len(dims) == 2, "dims must have only two keys: 'descr' and 'affine'"
    dims_chosen = dims.pop(preference) or next(iter(dims.values()))
    if dims_chosen is None:
        raise ValueError("Cannot infer dims, provide dims explicitly")
    return dims_chosen


def load_nii(
    path,
    dims=None,
    coords=None,
    attrs=None,
    coords_to_ras=False,
    data_to_ras=False,
    verbose=True,
    preferred_dim_source="affine",
):
    def map_coords(shape, spatial_dims, flip_map=None):
        spatial_dims = np.array(list(spatial_dims))
        if flip_map is None:
            # id map
            flip_map = dict(zip(spatial_dims, spatial_dims))
        dims_flipped = np.array([flip_map[dim] for dim in spatial_dims])
        dims_to_flip = dims_flipped != spatial_dims

        coords = {
            dim: np.arange(sz - 1, -1, -1) if flip else np.arange(sz)
            for dim, flip, sz in zip(dims_flipped, dims_to_flip, shape)
        }
        return coords

    assert not (
        coords_to_ras and data_to_ras
    ), "Cannot set both coords_to_ras and data_to_ras to True"
    img = nib.load(path)

    if data_to_ras:
        img = nib.as_closest_canonical(img)

    ndims = len(img.shape)
    if dims is None:
        # Dims are not specified. Try to infer
        dims = {}
        # 1. From description in the header, assuming it comes from MK's MRI Toolbox
        description = img.header["descrip"].item()
        try:
            dims["descr"] = _dims_from_descr(description, ndims)
        except AssertionError:
            dims["descr"] = None

        # 2. From the affine transform, assuming the last axis may be echo
        try:
            dims["affine"] = _dims_from_affine(img.affine, ndims)
        except AssertionError:
            dims["affine"] = None

        dims = _choose_dims(dims, preferred_dim_source)
        if verbose:
            print(f"Dims are not specified. Identified as {dims}")
    else:
        assert ndims == len(
            dims
        ), f"Conflicting sizes: the data are {ndims}-dim but {len(dims)} dims are passed"

    if coords_to_ras:
        flip_to_ras = {"I": "S", "L": "R", "P": "A", "S": "S", "R": "R", "A": "A"}
        spatial_coords = map_coords(img.shape[:3], dims[:3], flip_to_ras)
        dims = tuple(spatial_coords.keys()) + dims[3:]
        if verbose:
            print(f"Dims are mapped to {dims}")
    else:
        spatial_coords = map_coords(img.shape[:3], dims[:3])

    non_spatial_coords = {
        dim: np.arange(sz) for dim, sz in zip(dims[3:], img.shape[3:])
    }
    _coords = {**spatial_coords, **non_spatial_coords}

    if coords is not None:
        _coords.update(coords)

    da = xr.DataArray(img.get_fdata(), dims=dims, coords=_coords)
    da.attrs["affine"] = img.affine
    if attrs is not None:
        da.attrs.update(attrs)

    return da


def load_dicom(input_path, user_maps=None, reorder_to_ras=True):
    """Follows the provided directory and assembles a dataset based on the
    default and user defined mappings from DICOM field name to an xarray dimension"""

    # Sort out mappings: user vs. default
    if user_maps is None:
        user_maps = OrderedDict()
    maps = OrderedDict(
        [
            ("echo", (dm.fetch_func_factory("EchoTime"), dm.default_sorting_func)),
            ("slice", (dm.fetch_slice_location, dm.default_sorting_func)),
        ]
    )
    maps.update(user_maps)
    # Read dicoms into a list
    dcms = _read_dcm_input(input_path)
    # Assemble a conventional xr.DataArray
    data_array = _assemble_volume(dcms, maps, reorder_to_ras=reorder_to_ras)
    return data_array


def _read_dcm_input(input_path):
    """If input_path is a directory, reads all dicoms in there and returns them as a list.
    If input_path is a file, reads it and returns a list with a single element.
    If input_path is a string of filenames, reads all and returns them as a list."""
    # I know, it's writen in a very not Pythonic way
    if type(input_path) is list:
        files_not_found = [
            file_name for file_name in input_path if not os.path.exists(file_name)
        ]
        assert not files_not_found, "No such file or directory: {}".format(
            ", ".join(files_not_found)
        )
        dicom_file_list = input_path
    elif type(input_path) is str:
        try:
            dicom_file_list = [
                os.path.join(input_path, file_name)
                for file_name in os.listdir(input_path)
                if os.path.isfile(os.path.join(input_path, file_name))
            ]
        except NotADirectoryError:
            assert os.path.exists(input_path), "No such file or directory: {}".format(
                input_path
            )
            dicom_file_list = [input_path]
    else:
        raise ValueError(
            "Expected either a path to a folder or a file, or a list of pathes to files"
        )

    dcms = [
        pydicom.filereader.read_file(dcm_filename, force=True)
        for dcm_filename in dicom_file_list
    ]
    return dcms


def _assemble_volume(dcms, dcm_maps, reorder_to_ras=False):
    """Sorts provided DICOMs according to the mappings given

    Parameters
    ----------
    dcms : list of dicom.dataset.FileDataset objects
    dcm_maps : an OrderedDict mapping dimensions' names to generating functions
        generating function accepts a list of dicom.dataset.FileDataset objects
        and returns a dict {unique_scalar_value: unique_index})

    Returns
    -------
    data_array : xarray.DataArray object with .data field being the actual data and .dims build via dcm_maps
    """
    fields_values = {
        dim_name: [fetch_func(dcm) for dcm in dcms]
        for dim_name, (fetch_func, _) in dcm_maps.items()
    }
    field_maps = OrderedDict(
        [
            (dim_name, sort_func(fields_values[dim_name]))
            for dim_name, (_, sort_func) in dcm_maps.items()
        ]
    )
    field_maps, dcm_maps = _remove_singleton_dims(field_maps, dcm_maps)
    size_volume = _infer_array_size(dcms[0], field_maps)
    data = _fill_array(size_volume, dcms, field_maps, dcm_maps)
    np_axis2xr_dim = _get_matrix_labels(dcms[0])
    dims, coords = _produce_array_labels(size_volume, field_maps, np_axis2xr_dim)
    data_array = xr.DataArray(data, dims=dims, coords=coords)

    if reorder_to_ras:
        # DICOM uses LPS+ system (a.k.a. LPH+), while NIFTI uses RAS+ system
        pcs2nii = {
            "L": ("R", True),
            "P": ("A", True),
            "H": ("S", False),
            "R": ("R", False),
            "A": ("A", False),
            "F": ("S", True),
        }  # second value in tuple represents if the dim is flipped
        xr2pcs = _get_xr2pcs_map(dcms[0], np_axis2xr_dim)
        xr_dim_to_flip = [
            xr_dim for xr_dim, pcs_dim in xr2pcs.items() if pcs2nii[pcs_dim][1]
        ]

        # Flip np array (or return a view, not sure yet...)
        idx = {dim: data_array.coords[dim][::-1] for dim in xr_dim_to_flip}
        data_array = data_array.loc[idx]
        # Flip xr coords
        for dim in xr_dim_to_flip:
            data_array.coords[dim] = np.flip(data_array.coords[dim], axis=0)
    return data_array


def _remove_singleton_dims(field_maps, dcm_maps):
    """Removes the mri dimensions which correspond to a single value in dicom files

    Parameters
    ----------
    field_maps : an OrderedDict representing value mapping {dim_name: {unique_value: unique_idx}}
    dcm_maps : a list of DcmMap objects representing abstract mappings

    Returns
    -------
    field_maps : a dictionary representing value mapping {dim_name: {unique_value: unique_idx}}
    dcm_maps : a list of DcmMap objects representing abstract mappings

    The function filters both dictionaries and removes mri_dims associated with a single unique_value
    """
    non_singleton_field_maps = OrderedDict(
        [(dname, fmap) for dname, fmap in field_maps.items() if len(fmap) > 1]
    )
    non_singleton_dcm_maps = OrderedDict(
        [(dname, dcm_maps[dname]) for dname in non_singleton_field_maps.keys()]
    )
    return non_singleton_field_maps, non_singleton_dcm_maps


def _infer_array_size(ref_dcm, field_maps):
    """Identifies the size of read, line and other mapped dimensions

    Parameters
    ----------
    ref_dcm : a single dicom file represented as a dicom.dataset.FileDataset objects
    field_maps : a dictionary representing value mapping {dim_name: {unique_value: unique_idx}}

    Returns
    -------
    size_volume : [n_of_col, n_of_rows, size_of_other_identified_dims]
    """

    size_mapped = [len(field_map) for field_map in field_maps.values()]
    size_matrix = [
        ref_dcm.data_element(
            "Rows"
        ).value,  # no of rows    == in numpy: size along the 0th (-2th) dim
        ref_dcm.data_element("Columns").value,
    ]  # no of columns == in numpy: size along the 1st (-1th) dim
    size_volume = size_mapped + size_matrix
    return size_volume


def _fill_array(size_volume, dcms, field_maps, dcm_maps):
    """Iterate through the found dcm files and map them into the proper location
    Parameters
    ----------
    size_volume : desired numpy array size
    dcms : list of dicom.dataset.FileDataset objects
    field_maps : a dictionary representing value mapping {dim_name: {unique_value: unique_idx}}
    dcm_maps : a dictionary representing abstract mappings {dim_name: (dicom_field, mapping_function)}

    Returns
    -------
    data : numpy array, created to match identified mappings in size
    """
    data = np.zeros(size_volume)
    for dcm in dcms:
        current_field_value = OrderedDict(
            [
                (dim_name, fetch_func(dcm))
                for dim_name, (fetch_func, _) in dcm_maps.items()
            ]
        )
        # map the vale to the image location
        idx_mapped = tuple(
            field_maps[dim_name][field_value]
            for dim_name, field_value in current_field_value.items()
        )
        # fills first two dimensions, while the rest are specified by idx_mapped
        try:
            data[np.index_exp[idx_mapped + (...,)]] = dcm.pixel_array
        except AttributeError:
            dcm.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian
            data[np.index_exp[idx_mapped + (...,)]] = dcm.pixel_array

    return data


def _produce_array_labels(size_volume, field_maps, np_axis2xr_dim):
    """Satisfy the xarray demands
    Parameters
    ----------
    size_volume : desired numpy array size
    field_maps : a dictionary representing value mapping {dim_name: {unique_value: unique_idx}}
    np_axis2xr_dim : a mapping from numpy axis indices (0 or 1) to xr_dims ('read' or 'line') (e.g. {1: 'read', 0:'line'})

    Returns
    -------
    dims : tuple of dimension names associated with this array
    coords : list of coordinate arrays
    """
    read_line_generator = (
        dim for _, dim in sorted(np_axis2xr_dim.items())
    )  # get dims (dict values) sorted via dict keys
    dims = tuple(itertools.chain(field_maps.keys(), read_line_generator))
    # is there a natural labeling to read and line? I am just using indices from 0
    default_coords = [
        list(range(size_volume[len(field_maps) + idx]))
        for idx in sorted(np_axis2xr_dim.keys())
    ]
    # while these are labeled naturally with the keys of each field_map dict sorted by the value of the dict
    user_coords = [
        [value for value, _ in sorted(field_map.items(), key=lambda tup: tup[1])]
        for field_map in field_maps.values()
    ]
    coords = user_coords + default_coords
    return dims, coords


def _get_matrix_labels(ref_dcm):
    """Identifies which matrix dimension is frequency encoding (read),
        and which is phase encoding(line)
    Parameters
    ----------
    ref_dcm : a single dicom file represented as a dicom.dataset.FileDataset objects

    Returns
    -------
    np_axis2xr_dim : a mapping from numpy dim indices to mri_dims (e.g. {1: 'read', 0:'line'})
    """
    # dcm_acquisition_matrix is frequency rows\frequency columns\phase rows\phase columns
    dcm_acquisition_matrix_convention = np.array(
        [("read", "row"), ("read", "column"), ("line", "row"), ("line", "column")],
        dtype=[("acq_dim", np.unicode_, 6), ("im_dim", np.unicode_, 6)],
    )
    # Stated explicidly as a separate level because I personally find it confusing
    numpy_dimension_convention = {"column": 0, "row": 1}

    # Getting actual data:
    dcm_acquisition_matrix = np.array(ref_dcm.data_element("AcquisitionMatrix").value)
    acquisition_matrix = {
        acq_dim: im_dim
        for acq_dim, im_dim in dcm_acquisition_matrix_convention[
            dcm_acquisition_matrix != 0
        ]
    }
    np_axis2xr_dim = {
        numpy_dimension_convention[im_dim]: acq_dim
        for acq_dim, im_dim in acquisition_matrix.items()
    }
    return np_axis2xr_dim


def _get_xr2pcs_map(ref_dcm, np_axis2xr_dim):
    positive_nii_directions = ["R", "A", "SH"]
    positive_pcs_directions = list(
        ref_dcm[0x00511013].value[1:]
    )  # current data in patient coordinate system
    flipped_pcs_directions = np.array(
        [
            pcs_dir not in nii_dir
            for pcs_dir, nii_dir in zip(
                positive_pcs_directions, positive_nii_directions
            )
        ]
    )
    row2pcs, col2pcs = (
        np.array(ref_dcm.ImageOrientationPatient, dtype=float)
        .round()
        .astype(int)
        .reshape((2, 3))
    )
    xr2pcs = {
        xr_dim: positive_pcs_directions[np_axis]
        for np_axis, xr_dim in np_axis2xr_dim.items()
    }
    xr2pcs.update({"slice": positive_pcs_directions[2]})
    return xr2pcs
